@extends('spg.app')

@section('main')
  <div class="container">
    <div class="row">
      <a href="{{url('/')}}" class="btn btn-link">Volver al Menú Principal</a>
      <h1>1. Scott, Pikin y Gatica</h1>
      <p class="text-justify">Scott y Pikin son perros, y los dos son enemigos, sin embargo los dos se relacionan con Gatica, que es una gata anónima sin dueño, a ninguno le parece desagradar estar en su compañía. Por lo que tendrás que imprimir los números del uno al cien y cada vez que salga un número par debes reemplazar el número con "es", cuando sea un múltiplo de tres por "Pikin", cuando sea un múltiplo de cinco por "Scott" y cuando sea un múltiplo de cinco y de tres por "Gatica". Cuando se cumplan varias condiciones se deben concatenar los resultados, por ejemplo: 30 sería "esPikinScottGatica".</p>
      <label class="label label-default">Solución:</label>
      <pre>
        $result = '';
        for ($i=1; $i <= 100; $i++) {
          $result.= ($i % 2 == 0) ? 'es':$i;
          $result.= ($i % 3 == 0) ? 'Pikin':'';
          $result.= ($i % 5 == 0) ? 'Scott':'';
          $result.= ($i % 3 == 0 and $i % 5 == 0) ? 'Gatica':'';
          $result.= '&lt;/br&gt;';
        }
        return view('partials.spg', ['result' => $result]);
      </pre>
      <label class="label label-success">Resultado:</label>
      <div class="alert alert-success" role="alert"><?= $result;?></div>
    </div>
  </div>
@stop